const data = {
    posts: [
        {
            _id: 1,
            userId: 2,
            caption: "captionnya",
            tags: "tagsnya",
            likes: 0,
            image: "url"
        },
        {
            _id: 2,
            userId: 2,
            caption: "captionnya",
            tags: "tagsnya",
            likes: 0,
            image: "url"
        }
    ]
}

export default data;
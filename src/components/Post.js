import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { listPosts } from '../actions/PostActions';
import { instanceAxios } from '../config/AxiosConfig';

export default function Post(props) {
    const { post } = props;
    const [liked, setLike] = useState(false);

    const dispatch = useDispatch();

    const likeData = async (postId) => {
        try {
            const like = await instanceAxios.put(`/post/like/${postId}`);
            console.log(like.data.message);
            setLike(true);
            // dispatch(listPosts());
        } catch (error) {
            console.log(error.response.data.message);
        }
    }

    const deletePostData = async (postId) => {
        try {
            const deleteData = await instanceAxios.delete(`/post/${postId}`);
            console.log(deleteData);
        } catch (error) {
            console.log(error.response.data.message);
        }
    }
    return (
        <div key={post.id} className="card">
            <img className="medium" src={post.image} alt="post" />
            <div className="card-body">
                <p><button disabled={liked} onClick={() => likeData(post.id)}>Like</button> {post.likes}</p>
                <p>{post.user.username}</p>
                <p>{post.caption}</p>
                <p>{post.tags}</p>
            </div>
            <Link to={`/post/${post.id}`}>
                <button>Edit</button> <br />
            </Link>
            <button onClick={() => { if (window.confirm('Are you sure want to delete this data?')) { deletePostData(post.id) } }}>Delete</button>
        </div>
    )
}
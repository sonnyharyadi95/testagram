import { applyMiddleware, combineReducers, compose, createStore } from 'redux'
import thunk from 'redux-thunk';
import { PostDetailReducer, PostListReducer } from './reducers/PostReducers';

const initialState = {};
const reducer = combineReducers({
    postList: PostListReducer,
    postDetails: PostDetailReducer

})


// const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(reducer, initialState, compose(applyMiddleware(thunk)));

export default store;
import Axios from "axios";
import { BASE_URL } from "./Constant";
export const instanceAxios = Axios.create({
    baseURL: BASE_URL
});
import { instanceAxios } from "../config/AxiosConfig";
import { POST_LIST_FAIL, POST_LIST_REQUEST, POST_LIST_SUCCESS, POST_DETAILS_REQUEST, POST_DETAILS_SUCCESS, POST_DETAILS_FAIL } from "../config/PostConstants"

export const listPosts = () => async (dispatch) => {
    dispatch({
        type: POST_LIST_REQUEST
    });
    try {
        const { data } = await instanceAxios.get('/post?page=1&limit=10&searchBy=tags&search=');
        dispatch({ type: POST_LIST_SUCCESS, payload: data.data })
    } catch (error) {
        dispatch({ type: POST_LIST_FAIL, payload: error.message });
    }
}

export const detailPost = (postId) => async (dispatch) => {
    dispatch({ type: { POST_DETAILS_REQUEST, payload: postId } });
    try {
        const { data } = await instanceAxios.get(`post/${postId}`);
        dispatch({ type: POST_DETAILS_SUCCESS, payload: data.data })
    } catch (error) {
        dispatch({ type: POST_DETAILS_FAIL, payload: error.response && error.response.data.message ? error.response.data.message : error.message })
    }
}
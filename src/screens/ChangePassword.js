import React, { useEffect, useState } from 'react';
import { instanceAxios } from '../config/AxiosConfig';

export default function ChangePassword() {
    const [oldPassword, setOldPassword] = useState('');
    const [newPasssword, setNewPassword] = useState('');
    const [confirmNewPassword, setConfirmNewPassword] = useState('');

    const handleSubmit = async (e) => {
        e.preventDefault();
        let formData = new FormData();
        formData.append("oldPassword", oldPassword);
        formData.append("newPassword", newPasssword);
        formData.append("confirmedNewPassword", confirmNewPassword);
        instanceAxios.put('/user/change-password', formData).then((response) => {
            console.log(response);
        }).catch((e) => console.log(e));
    }

    return (
        <div className='card login'>
            <form onSubmit={handleSubmit}>
                <input type="password" placeholder='old password' onChange={(e) => setOldPassword(e.target.value)} /> <br />
                <input type="password" placeholder='new password' onChange={(e) => setNewPassword(e.target.value)} /> <br />
                <input type="password" placeholder='confirm new password' onChange={(e) => setConfirmNewPassword(e.target.value)} /> <br />
                <button type="submit"> Submit </button>
            </form>
        </div>
    )
}
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { listPosts } from "../actions/PostActions";
import LoadingBox from "../components/LoadingBox";
import MessageBox from "../components/MessageBox";
import Post from '../components/Post';
import useToken from "../config/useToken";


export default function HomeScreen() {
    const { token, setToken } = useToken();
    if (!token) {
        return <LoginScreen setToken={setToken} />
    }
    const dispatch = useDispatch();
    const postList = useSelector(state => state.postList);
    const { loading, error, posts } = postList;
    useEffect(() => {
        dispatch(listPosts());
    }, [])
    return (
        <div>
            {
                loading ? (<LoadingBox></LoadingBox>)
                    :
                    error ? (<MessageBox variant="danger">{error}</MessageBox>)
                        : (<div className="row center">
                            {
                                posts.map((post) => (
                                    <Post key={post.id} post={post}></Post>
                                ))
                            }
                        </div>)
            }

        </div>

    )
}
import React, { useEffect, useState } from "react";
import { useParams, Link } from "react-router-dom";
import { instanceAxios } from "../config/AxiosConfig";
import MessageBox from "../components/MessageBox";
import LoadingBox from "../components/LoadingBox";

export default function PostUpdate() {
    let { id } = useParams();
    const [loading, setLoading] = useState(false);
    const [imgPreview, setImgPreview] = useState('');
    const [imgFile, setImgFile] = useState('');
    const [caption, setCaption] = useState('');
    const [tags, setTags] = useState('');
    const [error, setError] = useState(false);
    useEffect(() => {
        const fetchData = async () => {
            try {
                setLoading(true)
                const { data } = await instanceAxios.get(`/post/${id}`).then((post) => post.data);
                setLoading(false);
                setImgPreview(data.image);
                setCaption(data.caption);
                setTags(data.tags);
            } catch (error) {
                setError(error.message);
            }
        }
        fetchData();
    }, [])

    const submitHandler = async () => {

        try {
            let formDataUpload = new FormData();
            let urlUploadPath = "";
            if (imgFile) {
                formDataUpload.append("files", imgFile);
                const resultImgUpload = await instanceAxios.post(`/upload`, formDataUpload, {
                    headers: { "Content-Type": "multipart/form-data" }
                });
                urlUploadPath = resultImgUpload.data.data.url;
            }

            let formData = new FormData();
            formData.append("image", urlUploadPath ? urlUploadPath : imgPreview);
            formData.append("caption", caption);
            formData.append("tags", tags);
            const resultUpdate = await instanceAxios.put(`/post/${id}`, formData);
            console.log(resultUpdate);

        } catch (error) {
            console.log(error);
            setError(error.response.data.message);
        }
    }
    return (
        <div>
            {
                loading ? (<LoadingBox></LoadingBox>)
                    :
                    error ? (<MessageBox variant="danger">{error}</MessageBox>)
                        : error ? (<MessageBox>{error}</MessageBox>)
                            : (<div><Link to="/">Back</Link>
                                <form>
                                    <div className="card">
                                        <img className="medium" src={imgPreview} alt="post" />
                                        <div className="card-body">
                                            <input type="file" onChange={(e) => {
                                                setImgPreview(URL.createObjectURL(e.target.files[0]))
                                                setImgFile(e.target.files[0]);
                                            }} /> <br />
                                            Caption: <input type="text" value={caption} onChange={(e) => setCaption(e.target.value)} /> <br />
                                            Tags: <input type="text" value={tags} onChange={(e) => setTags(e.target.value)} />
                                        </div>
                                        <button type="button" onClick={submitHandler}> Update Data </button>
                                    </div>
                                </form>
                            </div>
                            )
            }

        </div>

    )
}
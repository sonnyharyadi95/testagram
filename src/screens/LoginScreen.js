import React, { useState } from 'react';
import { instanceAxios } from '../config/AxiosConfig';
import PropTypes from 'prop-types';

export default function LoginScreen({ setToken }) {
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [error, setError] = useState(false)

    const handleSubmit = (e) => {
        e.preventDefault()
        let formData = new FormData();
        formData.append("username", username);
        formData.append("password", password);
        instanceAxios.post(`/user/login`, formData)
            .then((response) => {
                if (response.status == 200) {
                    const token = response.data.data.token;
                    setToken(token);
                }
            }).catch((e) => {
                console.log(e)
                // setError(e.response.data.message)
            })
    }
    return (
        <div className='card login'>
            {error ? (<div>{error}</div>) : <div></div>}
            <form onSubmit={handleSubmit}>
                <input type="username" required value={username} placeholder="Input Username" id="username" name="username" onChange={(e) => setUsername(e.target.value)} /> <br />
                <input type="password" required value={password} placeholder="Input password" id="password" name="password" onChange={(e) => setPassword(e.target.value)} /> <br />
                <button type="submit"> Login </button>
            </form>
        </div>
    )
}

LoginScreen.prototype = {
    setToken: PropTypes.func.isRequired
}
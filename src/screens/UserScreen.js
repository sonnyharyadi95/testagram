import React, { useEffect, useState } from 'react';
import { instanceAxios } from '../config/AxiosConfig';

export default function UserScreen() {

    const [name, setName] = useState('');
    const [username, setUsername] = useState('');
    const [email, setEmail] = useState('');
    const [imgPreview, setImgPreview] = useState('');
    const [imgFile, setImgFile] = useState('');
    const [error, setError] = useState(false);
    useEffect(() => {
        instanceAxios.get('/user').then((user) => {
            const data = user.data.data;
            setName(data.name);
            setUsername(data.username);
            setEmail(data.email);
            setImgPreview(data.photo);
        }).catch((e) => console.log(e));
    }, [])


    const handleSubmit = async (e) => {
        e.preventDefault();
        let urlUploadPath = "";
        if (imgFile) {
            let formDataUpload = new FormData();
            formDataUpload.append("files", imgFile);
            const resultImgUpload = await instanceAxios.post(`/upload`, formDataUpload, {
                headers: { "Content-Type": "multipart/form-data" }
            });
            urlUploadPath = resultImgUpload.data.data.url;
        }

        let formData = new FormData();
        formData.append("name", name);
        formData.append("username", username);
        formData.append("email", email);
        formData.append("photo", urlUploadPath ? urlUploadPath : imgPreview);

        instanceAxios.put(`/user`, formData)
            .then((response) => {
                console.log(response);
            }).catch((e) => {
                console.log(e)
                // setError(e.response.data.message)
            })
    }
    return (
        <div className='card login'>
            {error ? (<div>{error}</div>) : <div></div>}
            <form onSubmit={handleSubmit}>
                <input type="text" value={name} placeholder="name" onChange={(e) => setName(e.target.value)} /> <br />
                <input type="text" value={username} placeholder="username" onChange={(e) => setUsername(e.target.value)} /> <br />
                <input type="text" value={email} placeholder="email" onChange={(e) => setEmail(e.target.value)} /> <br />
                <input type="file" onChange={(e) => {
                    setImgPreview(URL.createObjectURL(e.target.files[0]))
                    setImgFile(e.target.files[0]);
                }} /> <br />
                <img className='medium' src={imgPreview} /> <br />
                <button type="submit"> Submit </button>
            </form>
        </div>
    )
}
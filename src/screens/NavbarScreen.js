import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import PostUpdate from '../screens/PostUpdate';
import UserScreen from '../screens/UserScreen';
import ChangePassword from '../screens/ChangePassword';
import { Route, Routes } from 'react-router-dom';
import HomeScreen from './HomeScreen';

export default function Navbar() {
    return (
        <div id="wrapper">
            <div id="sidebar-wrapper">
                <ul className="sidebar-nav">
                    <li>
                        <Link to="/user">User</Link>
                    </li>
                    <li>
                        <Link to="/user/change-password">Change Password</Link>
                    </li>
                    <li>
                        <Link to="/">Post</Link>
                    </li>
                </ul>
            </div>
            <div id="page-content-wrapper">
                <Routes>
                    <Route path="/post/:id" element={<PostUpdate />}></Route>
                    <Route path="/" element={<HomeScreen />}></Route>
                    <Route path="/user" element={<UserScreen />} exact></Route>
                    <Route path="/user/change-password" element={<ChangePassword />} exact></Route>
                </Routes>
            </div>
        </div>
    )
}
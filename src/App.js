import React from 'react';

import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { ProtectedRoute } from './config/ProtectedRoutes';

import LoginScreen from './screens/LoginScreen';
import Navbar from './screens/NavbarScreen';
import RegisterScreen from './screens/RegisterScreen';
import useToken from './config/useToken';

function App() {
  const { token, setToken } = useToken();
  if (!token) {
    return <LoginScreen setToken={setToken} />
  }
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/login" element={<LoginScreen />} exact></Route>
        <Route path="/register" element={<RegisterScreen />} exact></Route>
        <Route path="/*" element={
          <ProtectedRoute user={token}>
            <Navbar />
          </ProtectedRoute>
        }></Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
